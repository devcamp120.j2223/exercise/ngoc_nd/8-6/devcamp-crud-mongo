//khai báo thư viện express
const express = require('express');
const { courseRouter } = require('./app/routes/couresRouter');
const { reviewRouter } = require('./app/routes/reviewRouter');

//khởi tạo ứng dụng nodejs
const app = new express();

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    urlencoded:true
}))

//khai báo port chạy nodejs
const port = 8000;

// app.get('/', (request, response) => {
//     let today = new Date();
//     console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`);
    
//     response.status(200).json({
//         message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
//     })
// })

//sử dụng router
app.use('/', courseRouter);
app.use('/', reviewRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})